package StackTrekActivity.E16dot2.Controller;

import StackTrekActivity.E16dot2.Repository.Student;
import StackTrekActivity.E16dot2.Service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/student")
public class StudentController {
    @Autowired
    private StudentService studentService;

//    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/createStudent")
    public Student createStudent(@RequestBody Student student){
        return studentService.createStudent(student);
    }

//    @PreAuthorize("hasAnyRole('ADMIN, USER')")
    @GetMapping("/getAllStudents")
    public List<Student> getAllStudents(){
        return studentService.getAllStudents();
    }

//    @PreAuthorize("hasAnyRole('ADMIN, USER')")
//    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/getStudent")
    public Student getStudent(@RequestParam long id){
        return studentService.getStudent(id);
    }

//    @PreAuthorize("hasRole('ADMIN')")
    @PutMapping("/updateStudent/{id}")
    public Student updateStudent(@PathVariable long id, @RequestBody Student student){
        return studentService.updateStudent(id, student);
    }

//    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/delete/{id}")
    void deleteStudent(@PathVariable long id){
        studentService.deleteStudent(id);
    }
}
