package StackTrekActivity.E16dot2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class E16dot2Application {

	public static void main(String[] args) {
		SpringApplication.run(E16dot2Application.class, args);
	}

}
