package StackTrekActivity.E16dot2.Config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;

import java.util.List;

@Configuration
@EnableWebSecurity
public class SecurityConfig {

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http.authorizeHttpRequests()
                .requestMatchers(HttpMethod.GET, "/api/student/**").hasAnyRole("USER", "ADMIN")
                .requestMatchers(HttpMethod.POST, "/api/student/**").hasRole("ADMIN")
                .requestMatchers(HttpMethod.PUT, "/api/student/**").hasRole("ADMIN")
                .requestMatchers(HttpMethod.DELETE, "/api/student/**").hasRole("ADMIN")
                .anyRequest().authenticated()
                .and()
                .csrf()
                .disable()
                .httpBasic(Customizer.withDefaults());
        return http.build();
    }

//    @Bean
//    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
//        http.authorizeHttpRequests()
//                .anyRequest()
//                .permitAll()
//                .and()
//                .csrf()
//                .disable()
//                .httpBasic(Customizer.withDefaults());
//        return http.build();
//    }

    @Bean
    public UserDetailsService userDetailsService() {
        UserDetails myAdmin = User.builder()
                .username("admin")
                .password(passwordEncoder().encode("admin"))
                .roles("ADMIN")
                .build();

        UserDetails myUser = User.builder()
                .username("user")
                .password(passwordEncoder().encode("user"))
                .roles("USER")
                .build();


        return new InMemoryUserDetailsManager(myAdmin, myUser);
    }

    @Bean
    public static PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

//    public UserDetailsService userDetailsService(){
//        List<UserDetailsService> userDetailsService;
//
//
//        userDetailsService.add(null);
//
//
//        return new InMemoryUserDetailsManager(userDetailsService);
//    }

//    @Bean
//    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
//        http.authorizeHttpRequests()
//                .anyRequest()
//                .permitAll()
//                .and()
//                .csrf()
//                .disable();
//        return http.build();
//    }

}
