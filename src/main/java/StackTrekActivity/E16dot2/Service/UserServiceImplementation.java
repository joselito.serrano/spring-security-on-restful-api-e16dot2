//package StackTrekActivity.E16dot2.Service;
//
//import StackTrekActivity.E16dot2.Exception.UsernameNotFoundException;
//import StackTrekActivity.E16dot2.Repository.User;
//import StackTrekActivity.E16dot2.Repository.UserRepository;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.core.authority.SimpleGrantedAuthority;
//import org.springframework.security.core.userdetails.UserDetailsService;
//
//import java.util.Collections;
//
////public class UserServiceImplementation implements UserService{
////    @Autowired
////    private UserRepository userRepository;
////
////    @Override
////    public UserDetailsService loadUserByUsername(String username) {
////        User user = (User) userRepository.findByUsername(username)
////                .orElseThrow(() -> new UsernameNotFoundException("Student not found with username: " + username));
////
////        return new org.springframework.security.core.userdetails.User(user.getUsername(),
////                user.getPassword(), Collections.singletonList(new SimpleGrantedAuthority(user.getRole())));
////
////    }
////}
