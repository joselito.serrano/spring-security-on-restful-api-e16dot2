package StackTrekActivity.E16dot2.Service;

import StackTrekActivity.E16dot2.Repository.Student;
import StackTrekActivity.E16dot2.Repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class StudentServiceImplementation implements StudentService{

    @Autowired
    public StudentRepository studentRepository;

    @Override
    public Student createStudent(Student student) {
        return studentRepository.save(student);
    }
    @Override
    public List<Student> getAllStudents() {
        return studentRepository.findAll();
    }

    @Override
    public Student getStudent(long id) {
        return studentRepository.findById(id).orElseThrow(()-> new RuntimeException("Student does not exist"));
    }

    @Override
    public Student updateStudent(long id, Student student) {
        student.setId(id);;
        return studentRepository.save(student);
    }

    @Override
    public void deleteStudent(long id) {
        studentRepository.deleteById(id);
    }

}
