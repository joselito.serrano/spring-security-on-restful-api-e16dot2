package StackTrekActivity.E16dot2.Service;

import StackTrekActivity.E16dot2.Repository.Student;
import org.springframework.stereotype.Service;

import java.util.List;


public interface StudentService {

    Student createStudent(Student student);
    List<Student> getAllStudents();
    Student getStudent(long id);
    Student updateStudent(long id, Student student);
    void deleteStudent(long id);

}
